const {createApp} = Vue;
createApp({
    data(){
        return{
            randomIndex: 0,
            randomIndexInternet: 0,

            //vetor de imagens locais
            imagensLocais:[

                './Imagens/lua.jpg',
                './Imagens/SENAI_logo.png',
                './Imagens/sol.jpg'
            ],

            imagensInternet:[
                'https://img.freepik.com/vetores-gratis/bela-casa_24877-50819.jpg', 
                'https://blog.funstock.com.br/wp-content/uploads/2018/06/curiosidades-sobre-o-snoopy.png',
                'https://cdnv2.moovin.com.br/armazemdomercado/imagens/produtos/det/po-para-sorvete-coco-du-porto-1kg-e49e2fb84c1cbc1e3d4b85f4fe122cbb.png'

            ],
        };//fim return
    },//fim data

    computed:{
        randomImage()
        {
            return this.imagensLocais[this.randomIndex];//move as imagens 
        },//fim randomImage

        randomImageInternet()
        {
            return this.imagensInternet[this.randomIndexInternet];
        },//fim do randomImageInternet
    },//fim computed

    methods:{
        getRandomImage()
        {
            this.randomIndex=Math.floor(Math.random()*this.imagensLocais.length);
            this.randomIndexInternet=Math.floor(Math.random()*this.imagensInternet.length);
        },//fim do getRandomImage

        
    },//fim methods

    

}).mount("#app");