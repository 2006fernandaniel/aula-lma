const {createApp} = Vue;

createApp({
    data(){
        return{
            show:false, //variável
            itens:["Bola", "Bolsa", "Tenis"], //array
        };//Fechamento return
    },//Fechamento data

    methods:{
        showItens:function(){
            this.show = !this.show;

        },//Fechamento showItens
    },//Fechamento methods

}).mount("#app"); //Fechamento creatApp
