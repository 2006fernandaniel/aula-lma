const {createApp} = Vue;
createApp({
    data(){
        return{
            paginaInicial:"index.html",
        }; //fim return

    },//fechamento da função data

    methods:{
        loadPage: function(){
            window.location.href = 'pagina2.html';
        }//fim loadPage
    },//fim methods

}).mount("#app");