//Processamento dos dados do formulário"index.html"

//Acessando os elementos do formulário html
const formulario = document.getElementById ("formulario1");

//Adiciona um ouvinte de eventos a um elemeneto HTML
formulario.addEventListener("submit", function(evento)
{
    evento.preventDefault(); //previne o comportamento padrão de um elemento HTML em resposta a um evento

    //variáveis para trataros dados recebidos dos elementos do formulário
    const nome = document.getElementById("nome").value;
    const email = document.getElementById("email").value;

    //exibe um alerta com os dados coletados
    // alert(`Nome: ${nome} --- E-mail ${email}`);

    const updateResultado = document.getElementById("resultado");

    updateResultado.value = `Nome: ${nome} --- E-mail: ${email}`;

    updateResultado.style.width = updateResultado.scrollWidth + "px";
});