const {createApp} = Vue;
createApp({
    data(){
        return{
        testeSpan: false,
        isLampadaLigada: false,
    }//return
    },//Fim data

    methods:{
        handleTeste: function()
        {
            this.testeSpan = !this.testeSpan;
        },//fim handle test

        toggleLampada: function()
            {
                this.isLampadaLigada = !this.isLampadaLigada;
            },//Fim toggle lampada
        
    },//fim do methods

}).mount("#app");