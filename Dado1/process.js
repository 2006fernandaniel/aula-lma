const {createApp} = Vue;
createApp({
    data(){
        return{
            paginaInicial:"pagina2.html",
            paginaDado:"index.html",
            paginaAbout:"pagina3about.html",
            paginaDadoHeroi:"indexHeroi.html"
        }; //fim return

    },//fechamento da função data

    methods:{
        loadPageDado: function(){
            window.location.href = 'index.html';
        },//fim loadPage
        loadPageHome: function(){
            window.location.href = 'pagina2.html';
        },
        loadPageAbout: function(){
            window.location.href = 'pagina3about.html';
        },
        loadPageDadoHeroi: function(){
            console.log("teste");
            window.location.href = 'teste.html'
        }

    },//fim methods



}).mount("#app");

